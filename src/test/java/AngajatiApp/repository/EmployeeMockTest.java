package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.validator.EmployeeException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {

    private EmployeeMock  em;

    @BeforeEach
    void setup() {

        em = new EmployeeMock();
    }

    // one employee in the list + update employee
    @Test
    void modifyEmployee_TC01() {

        EmployeeMock emc = new EmployeeMock(1);

        Employee Ionela = new Employee(
                "Gicu",
                "Ionescu",
                "1234567890876",
                DidacticFunction.LECTURER,
                2500d);
        Ionela.setId(1);

        emc.modifyEmployeeFunction(Ionela, DidacticFunction.TEACHER);
        assertEquals(DidacticFunction.TEACHER, emc.getEmployeeList().get(0).getFunction());
    }

    // 4 employees in the list + update first employee
    @Test
    void modifyEmployee_TC02() {

        Employee Ionela = new Employee(
                "Gicu",
                "Ionescu",
                "1234567890876",
                DidacticFunction.LECTURER,
                2500d);
        Ionela.setId(1);

        em.modifyEmployeeFunction(Ionela, DidacticFunction.TEACHER);
        assertEquals(DidacticFunction.TEACHER, em.getEmployeeList().get(0).getFunction());
    }

    // 4 employees in the list + null parameter
    @Test
    void modifyEmployee_TC03() {

        EmployeeMock emc = new EmployeeMock("reverse");

        emc.modifyEmployeeFunction(null, DidacticFunction.TEACHER);

        assertEquals(DidacticFunction.ASISTENT, emc.getEmployeeList().get(0).getFunction());
        assertEquals(DidacticFunction.TEACHER, emc.getEmployeeList().get(1).getFunction());
        assertEquals(DidacticFunction.TEACHER, emc.getEmployeeList().get(2).getFunction());
        assertEquals(DidacticFunction.LECTURER, emc.getEmployeeList().get(3).getFunction());
    }

    // 0 employees in the list
    @Test
    void modifyEmployee_TC04() {

        EmployeeMock emc = new EmployeeMock('c');

        assertThrows(IndexOutOfBoundsException.class,
                () -> {
                    Employee Ionela = new Employee(
                            "Gicu",
                            "Ionescu",
                            "1234567890876",
                            DidacticFunction.LECTURER,
                            2500d);
                    emc.modifyEmployeeFunction(Ionela, DidacticFunction.TEACHER);
                    emc.getEmployeeList().get(0);
                });
    }

    // 4 employees in the list + employee that is not in the list
    @Test
    void modifyEmployee_TC05() {

        EmployeeMock emc = new EmployeeMock("reverse");

        Employee Maria = new Employee(
                "Maria",
                "Iova",
                "1234567890876",
                DidacticFunction.LECTURER,
                2500d);

        emc.modifyEmployeeFunction(Maria, DidacticFunction.TEACHER);

        assertEquals(DidacticFunction.ASISTENT, emc.getEmployeeList().get(0).getFunction());
        assertEquals(DidacticFunction.TEACHER, emc.getEmployeeList().get(1).getFunction());
        assertEquals(DidacticFunction.TEACHER, emc.getEmployeeList().get(2).getFunction());
        assertEquals(DidacticFunction.LECTURER, emc.getEmployeeList().get(3).getFunction());
    }

    // 4 employees in the list + update the last employee
    @Test
    void modifyEmployee_TC06() {

        EmployeeMock emc = new EmployeeMock("reverse");

        Employee Ionela = new Employee(
                "Gicu",
                "Ionescu",
                "1234567890876",
                DidacticFunction.LECTURER,
                2500d);
        Ionela.setId(4);

        emc.modifyEmployeeFunction(Ionela, DidacticFunction.TEACHER);
        assertEquals(DidacticFunction.TEACHER, emc.getEmployeeList().get(3).getFunction());
    }

    // cu date valide
    @Test
    void addEmployeeTC1_EC() {

        Employee e1 = new Employee(
                "firstName",
                "lastName",
                "2881056634921",
                DidacticFunction.TEACHER,
                3400.00);
        assertTrue(em.addEmployee(e1));
        //assertEquals(1, this.em.getEmployeeList().size());
    }

    // cu date nevalide (salary < 1300)
    @Test
    void addEmployeeTC2_EC_TC1_BVA() {

        Employee e1 = new Employee(
                "firstName",
                "lastName",
                "2881056634921",
                DidacticFunction.TEACHER,
                1299.00);
        assertFalse(em.addEmployee(e1));
        //assertEquals(0, this.em.getEmployeeList().size());
    }

    // cu date nevalide (salary > 9000)
    @Test
    void addEmployeeTC3_EC_TC6_BVA() {

        Employee e1 = new Employee(
                "firstName",
                "lastName",
                "2881056634921",
                DidacticFunction.TEACHER,
                9001.00);
        assertFalse(em.addEmployee(e1));
        //assertEquals(0, this.em.getEmployeeList().size());
    }

    // cu date nevalide (firstName == null)
    @Test
    void addEmployeeTC4_EC_TC7_BVA() {

        assertThrows(NullPointerException.class,
                () -> {
                    Employee e1 = new Employee(
                            null,
                            "lastName",
                            "2881056634921",
                            DidacticFunction.TEACHER,
                            9001.00);
                    em.addEmployee(e1);
                });
    }

    // cu date nevalide (firstName == "")
    @Test
    void addEmployeeTC5() {

        Employee e1 = new Employee(
                "",
                "lastName",
                "2881056634921",
                DidacticFunction.TEACHER,
                9001.00);
        assertFalse(em.addEmployee(e1));
        //assertEquals(0, this.em.getEmployeeList().size());
    }

    // cu date nevalide (firstName.length > 255)
    @Test
    void addEmployeeTC6_EC_TC12_BVA() {

        Employee e1 = new Employee(
                "firstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameiiiiiii",
                "lastName",
                "2881056634921",
                DidacticFunction.TEACHER,
                9001.00);
        assertFalse(em.addEmployee(e1));
        //assertEquals(0, this.em.getEmployeeList().size());
    }

    // cu date valide (firstName == "f" && salary == 1300.00)
    @Test
    void addEmployeeTC62BVA_TC8_BVA() {

        Employee e1 = new Employee(
                "f",
                "lastName",
                "2881056634921",
                DidacticFunction.TEACHER,
                1300.00);
        assertTrue(em.addEmployee(e1));
        //assertEquals(1, this.em.getEmployeeList().size());
    }

    // cu date valide (firstName == "fi" && salary == 1301.00)
    @Test
    void addEmployeeTC32BVA_TC9_BVA() {

        Employee e1 = new Employee(
                "fi",
                "lastName",
                "2881056634921",
                DidacticFunction.TEACHER,
                1301.00);
        assertTrue(em.addEmployee(e1));
        //assertEquals(1, this.em.getEmployeeList().size());
    }

    // cu date valide (firstName.length == 254 && salary == 8999.00)
    @Test
    void addEmployeeTC42BVA_TC10_BVA() {

        Employee e1 = new Employee(
                "firstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameiiiii",
                "lastName",
                "2881056634921",
                DidacticFunction.TEACHER,
                8999.00);
        assertTrue(em.addEmployee(e1));
        //assertEquals(1, this.em.getEmployeeList().size());
    }

    // cu date valide (firstName.length == 255 && salary == 9000.00)
    @Test
    void addEmployeeTC52BVA_TC11_BVA() {

        Employee e1 = new Employee(
                "firstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameifirstNameiiiiii",
                "lastName",
                "2881056634921",
                DidacticFunction.TEACHER,
                9000.00);
        assertTrue(em.addEmployee(e1));
        //assertEquals(1, this.em.getEmployeeList().size());
    }
}