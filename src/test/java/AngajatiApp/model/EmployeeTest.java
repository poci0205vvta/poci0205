package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.validator.EmployeeException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EmployeeTest {
    private Employee employee;
    private Employee e1;
    private Employee e2;
    private Employee e3;

    private String employeeDb;

    @BeforeEach
    public void setup() {
        this.employee = new Employee();
        this.e1 = new Employee("firstNameTest1", "lastNameTest1", "123456789", DidacticFunction.TEACHER, 3600.00);
        this.e2 = new Employee("firstNameTest2", "lastNameTest2", "987654321", DidacticFunction.ASISTENT, 2600.00);
        this.e3 = new Employee("firstNameTest3", "lastNameTest3", "234571376", DidacticFunction.CONFERENTIAR, 4200.00);

        employeeDb = "Ionel;Pacuraru;123456789;ASISTENT;2500;0";
    }

    @Disabled
    @Test
    void getFirstName() {
        //setup
        employee.setFirstName("firstName1");

        //assert
        assertEquals("firstName1", employee.getFirstName());
    }

    @Test
    @Order(1)
    void setFirstName() {
        //setup
        employee.setFirstName("firstNameTest");

        //assert
        assertEquals("firstNameTest", employee.getFirstName());
    }

    @Test
    @Order(2)
    void setLastName() {
        //setup
        employee.setLastName("lastNameTest");

        //assert
        assertEquals("lastNameTest", employee.getLastName());
    }

    @Test
    @Timeout(value = 20, unit = TimeUnit.MILLISECONDS)
    @Order(3)
    void setCnp() throws InterruptedException {

        try {
            TimeUnit.MILLISECONDS.sleep(1);
        } catch(InterruptedException e) {
            e.printStackTrace();
        }

        employee.setCnp("123456789");
        assertEquals("123456789", employee.getCnp());

    }

    @Test
    @Order(4)
    void setCnp_withAssertTimeout() {
        assertTimeout(Duration.ofMillis(10), () -> {

            employee.setCnp("123456789");
            assertEquals("123456789", employee.getCnp());

            Thread.sleep(1);
        });
    }

    @Test
    @Order(5)
    void employeeDetailsSetup() {
        //assert
        Assertions.assertAll(
                () -> assertEquals("firstNameTest1", e1.getFirstName()),
                () -> assertEquals("lastNameTest2", e2.getLastName()),
                () -> assertEquals(4200.00, e3.getSalary())
        );
    }

    @ParameterizedTest
    @ValueSource(doubles = {6550.00, 3800.00, 2390.00, 7900.00})
    @Order(6)
    void setSalary(double expectedSalary) {
        //setup
        employee.setSalary(expectedSalary);

        //assert
        assertEquals(expectedSalary, employee.getSalary());
    }

    @Test
    @Order(8)
    void getEmployeeFromString() throws EmployeeException{
        //assert
        assertThrows(EmployeeException.class,
                () -> {
                    Employee.getEmployeeFromString(employeeDb, 1);
                });
    }

    @Test
    @Order(7)
    void constructorTest() {
        //setup
        Employee e = new Employee("firstName1", "lastName1", "1236494792", DidacticFunction.TEACHER, 4000.00);

        //assert
        assertNotNull(e);
    }
}